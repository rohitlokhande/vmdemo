package com.example.vmdemo.data.remote.repository;

import com.example.vmdemo.data.remote.client.ApiClient;
import com.example.vmdemo.data.remote.model.Sample;

import retrofit2.Call;

public class DemoRepository {

    private static DemoRepository repositoryInstance;

    public static DemoRepository getInstance() {
        if (repositoryInstance == null) {
            repositoryInstance = new DemoRepository();
        }
        return repositoryInstance;
    }

    public Call<Sample> hitServer() {
        return ApiClient.getClient().getData();
    }

}
