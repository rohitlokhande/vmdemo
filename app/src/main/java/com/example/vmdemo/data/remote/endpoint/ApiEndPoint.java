package com.example.vmdemo.data.remote.endpoint;

import com.example.vmdemo.data.remote.model.Sample;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiEndPoint {

    @GET("demo")
    Call<Sample> getData();

}