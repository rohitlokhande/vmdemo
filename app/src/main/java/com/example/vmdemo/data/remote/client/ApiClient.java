package com.example.vmdemo.data.remote.client;

import com.example.vmdemo.data.remote.endpoint.ApiEndPoint;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    private static final String BASE_URL = "http://5cc08afc82ec6a00149f3cec.mockapi.io/";

    private static ApiEndPoint apiClient;

    public static ApiEndPoint getInterface() {
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder().addHeader("User-Agent", "Retrofit-Sample-App").build();
                return chain.proceed(newRequest);
            }
        };

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        /*
        Add the interceptor to OkHttpClient
         */
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        builder.retryOnConnectionFailure(false);
        builder.connectTimeout(1, TimeUnit.MINUTES);
        builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        OkHttpClient client = builder.build();

        /*
        Set the custom client when building adapter
         */
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client).build();

        return retrofit.create(ApiEndPoint.class);
    }

    public static ApiEndPoint getClient() {
        if (apiClient == null) {
            apiClient = getInterface();
        }
        return apiClient;
    }

}