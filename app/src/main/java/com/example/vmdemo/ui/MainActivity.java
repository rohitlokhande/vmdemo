package com.example.vmdemo.ui;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.vmdemo.R;

public class MainActivity extends AppCompatActivity {

    private DemoViewModel viewModel;
    private TextView tvId;
    private TextView tvName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initViewModel();
        observer();
    }

    private void initViews() {
        tvId = findViewById(R.id.txt_id);
        tvName = findViewById(R.id.txt_name);
    }

    private void initViewModel() {
        viewModel = ViewModelProviders.of(this).get(DemoViewModel.class);
        viewModel.hitServerForData();
    }

    private void observer() {

        viewModel.getSampleMutableLiveData().observe(this, data -> {
            if (data != null) {
                tvId.setText(data.getId());
                tvName.setText(data.getName());
            }
        });

    }
}
