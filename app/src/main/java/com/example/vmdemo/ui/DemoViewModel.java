package com.example.vmdemo.ui;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.vmdemo.data.remote.model.Sample;
import com.example.vmdemo.data.remote.repository.DemoRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DemoViewModel extends ViewModel {

    private MutableLiveData<Sample> sampleMutableLiveData;

    public DemoViewModel() {
        sampleMutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<Sample> getSampleMutableLiveData() {
        return sampleMutableLiveData;
    }

    public void setSampleMutableLiveData(MutableLiveData<Sample> sampleMutableLiveData) {
        this.sampleMutableLiveData = sampleMutableLiveData;
    }

    public void hitServerForData(){
        DemoRepository.getInstance().hitServer().enqueue(callback);
    }

    final Callback<Sample> callback = new Callback<Sample>() {
        @Override
        public void onResponse(Call<Sample> call, Response<Sample> response) {
            if(response.isSuccessful()){
                sampleMutableLiveData.setValue(response.body());
            }
        }

        @Override
        public void onFailure(Call<Sample> call, Throwable t) {

        }
    };


}
